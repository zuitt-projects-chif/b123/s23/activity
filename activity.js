db.users.insertMany([
  {
    firstName: "Chris",
    lastName: "Henry",
    email: "chris.henry@example.com",
    password: "qweqwe",
    isAdmin: false,
  },
  {
    firstName: "Lessa",
    lastName: "Torres",
    email: "lesa.torres@example.com",
    password: "emerald",
    isAdmin: false,
  },
  {
    firstName: "Aubrey",
    lastName: "Martin",
    email: "aubrey.martin@example.com",
    password: "guavadb",
    isAdmin: false,
  },
  {
    firstName: "Jack",
    lastName: "Bryant",
    email: "jack.bryant@example.com",
    password: "bigdog",
    isAdmin: false,
  },
  {
    firstName: "Aaron",
    lastName: "Jennings",
    email: "aaron.jennings@example.com",
    password: "taylor1",
    isAdmin: false,
  },
]);

db.courses.insertMany([
  {
    name: "Intro to HTML",
    description: "An introductory course to HyperText Markup Language",
    price: 1299,
    isActive: true,
  },
  {
    name: "Intro to CSS",
    description: "An introductory course to Cascading Style Sheets ",
    price: 1399,
    isActive: true,
  },
  {
    name: "Intro to JS",
    description: "An introductory course to Javascript",
    price: 1599,
    isActive: true,
  },
]);

db.users.updateOne({}, { $set: { isAdmin: true } });

db.users.find({ isAdmin: false });

db.courses.updateOne({}, { $set: { isActive: false } });
